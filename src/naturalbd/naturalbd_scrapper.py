import requests
from bs4 import BeautifulSoup as bs
# from datetime import datetime as dt
import time

# Categories
hm = 'http://hm.naturalbd.com'
bm = 'http://bm.naturalbd.com'
cm = 'http://cm.naturalbd.com'
anime = 'http://anime.naturalbd.com'
tamil = 'http://tamil.naturalbd.com'

# user inputs
selected_category = hm
# year = 2018

video_ext = ('.mkv', '.mp4', '.avi')
millis = str(int(round(time.time() * 1000 * 60))) + '_'


def collect_data(file_dir, files):
    cat = file_dir[7:file_dir.find('.')]  # .replace('http://', '')
    filtered_elements = [f for f in files if f['href'].endswith(video_ext)]
    if filtered_elements:
        with open(millis + cat + '.txt', "a", encoding='utf-8') as doc:
            movies_data = [f.text + ',' + file_dir + f['href'] for f in filtered_elements]
            doc.write('\n'.join(movies_data) + '\n')
            return True


def extract_elements(url):
    try:
        req = requests.get(url)
        soup = bs(req.text, 'html.parser')
        return soup.find_all('a')[1:]
    except ConnectionError or TimeoutError:
        print('Couldn\'t Connect. Retrying...')
        # extract_elements(url)


if __name__ == '__main__':
    years_dir = extract_elements(selected_category)
    trimmed_years = list(reversed(years_dir))[1:]
    for year in trimmed_years:
        year_path = selected_category + '/' + year.text
        movies = extract_elements(year_path)

        movie_folders = [m for m in movies if m.text.endswith('/')]
        movie_files = [m for m in movies if not m.text.endswith('/')]
        print(year.text, ' - ', len(movie_folders), 'folders,', len(movie_files), 'files')

        current_path = selected_category + '/' + year.text

        if movie_files:
            collect_data(current_path, movie_files)

        if movie_folders:
            for i, m in enumerate(movie_folders):
                folder_path = current_path + m['href']
                print('%d/%d' % (i + 1, len(movie_folders)), folder_path)
                collect_data(folder_path, extract_elements(folder_path))

import csv, os.path
from bs4 import BeautifulSoup as bs
import src.scrapping_utilities as util

details = 'https://basis.org.bd/index.php/members_area/member_detail/1150'
members = 'https://basis.org.bd/index.php/members_area/member_list'

# file names
file_member_links = 'member_links.tsv'
file_final_export = 'basis_member_details.tsv'
file_failed_links = 'failed_links.txt'


def scrap_members(url):
    # with open(file_path2, 'r') as myfile:
    #     data = myfile.read().replace('\n', '')
    #     soup = bs(data, 'html.parser')

    data = []
    tds = util.get_soup(url).find_all('td', {'class': 'bodytext', 'colspan': '2', 'align': 'left'})
    for i, e in enumerate(tds):
        if e.find('a') and 'Web:' not in e.text:
            line = '{}\t{}\t{}\t{}'.format(e.text, tds[i + 1].text, tds[i + 2].text, e.find('a')['href'])
            # line = " ".join(line.split())  # removes double space
            data.append(line)
            print(line)

    return data


def scrap_details(url):
    # with open(file_path, 'r') as myfile:
    #     data = myfile.read().replace('\n', '')
    #     soup = bs(data, 'html.parser')

    tds = util.get_soup(url).find_all('td', {'class': 'bodytext'})

    name = ''
    designation = ''
    contact = []
    email = []
    address = ''

    pass_next = False
    for i, td in enumerate(tds):
        if pass_next:
            pass_next = False
            continue

        cur = td.text.strip()
        # print('{}/{}.'.format(i + 1, len(tds)), '\'{}\''.format(cur))

        if i + 1 is len(tds): break
        nxt = tds[i + 1].text.strip()
        if not cur or not nxt: continue

        if cur == 'Name':
            name = nxt
        elif cur == 'Designation':
            designation = nxt
        elif 'Mobile' in cur or 'Contact No.' in cur:
            contact.append(nxt)
        elif 'mail' in cur:
            email.append(nxt)
        elif cur == 'Address':
            address = nxt
        else:
            pass_next = False
            continue
        pass_next = True

    # con_per_idx = [i for i, e in enumerate(tds) if e.text.strip() == 'Contact Person'][0]
    # trimmed = tds[con_per_idx + 1:]
    # print(trimmed)

    email = ', '.join(set(e.strip() for e in email if '@' in e))
    contact = ' '.join(contact).replace('Phone: ', '').replace('+', '').replace(',', '')
    contact = ', '.join(i for i in set(contact.split()) if i.isdigit() and len(i) > 6)  # removes double space
    address = address.replace('\n', ', ')
    return '{}\t{}\t{}\t{}\t{}'.format(name, designation, contact, email, address)


def collect_member_links(start=0, end=55):
    page_links = ['{}/{}'.format(members, i * 20) for i in range(start, end)]

    with open(file_member_links, "a", encoding='utf-8') as doc:
        for p in page_links:
            print('URL:', p)
            try:
                data = scrap_members(p)
                doc.write('\n'.join(data) + '\n')
            except:
                print('!!!---TIMEOUT ERROR----!!!' + p)


def collect_details():
    if not os.path.isfile(file_member_links):
        print('\'{}\' couldn\'t found.\nPlease collect member links first!'.format(file_member_links))
        return

    with open(file_member_links, 'rt') as tsv, open(file_failed_links, "w", encoding='utf-8') as err_doc:
        reader = csv.reader(tsv, delimiter='\t')

        # total = sum(1 for _ in reader) 10:16:15

        for i, row in enumerate(reader):
            # if i == 5: break
            print('{}/{}. URL:'.format(i + 1, 1086), row[3])
            try:
                detail = scrap_details(row[3])
                # phn = row[1].lstrip('Phone: ')
                web = row[2].lstrip('Web: ')
                line = '{}\t{}\t{}\t{}\n'.format(row[0], detail, web, row[3])
                # print('details:', line, end='')
                with open(file_final_export, "a", encoding='utf-8') as doc:
                    doc.write(line)
            except:
                print('!!!---TIMEOUT ERROR----!!!' + row[3])
                err_doc.write('\t'.join(row) + '\n')


if __name__ == '__main__':
    # collect_member_links()
    collect_details()
    # scrap_details(details)

import requests
from bs4 import BeautifulSoup as bs


def get_soup(url, html_file_path=None):
    if html_file_path:
        with open(html_file_path, 'r') as f:
            html = f.read().replace('\n', '')
    else:
        html = requests.get(url).content
    return bs(html, 'html.parser') if html else None


def collect_emails(raw_str):
    return set(e for e in raw_str.split() if '@' in e)


def strip_double_space(raw_str):
    return ' '.join(raw_str.split())

import csv, os.path
import src.utils.utils as utils

details_url = 'http://www.bgmea.com.bd/member/details/23910'
members_url = 'http://www.bgmea.com.bd/member/memberlist'
htm = 'D:\Other Things\Desktop\members.htm'

# file names
file_links = 'links.tsv'
file_final = 'res\\final.tsv'
file_failed = 'res\\failed.tsv'


def scrap_links(url):
    data = []
    trs = utils.get_soup(url).find_all('tr', {'class': 'tr_01'})
    for row in trs:
        tds = row.find_all('td')
        company = tds[0].text.strip()
        person = tds[2].text.strip()
        email = tds[3].text.strip()
        detail = tds[4].find('a')['href']
        line = '{}\t{}\t{}\t{}'.format(company, person, email, detail)
        data.append(line)
        # print(line)

    return data


def collect_links(links=None, start=0, end=218):
    page_links = links if links else ['{}/{}'.format(members_url, i * 20) for i in range(start, end)]

    for p in page_links:
        print('URL:', p)
        try:
            data = scrap_links(p)
            with open(file_links, "a", encoding='utf-8') as doc:
                doc.write('\n'.join(data) + '\n')
        except:
            print('!!!---TIMEOUT ERROR----!!!', p)


def scrap_details(url):
    soup = utils.get_soup(url)
    # soup = utils.get_soup(url, htm)  # test from local

    director = soup.find('tr', {'id': 'director_row0'}).find_all('td')
    name = director[1].text.strip()
    designation = director[0].text.strip()

    td = soup.find_all('td', string='Phone')
    contact = [director[2].text.strip()] + [i.parent.find_all('td')[1].text.strip() for i in td]
    contact = ['0' + c if len(c) in [10, 8] else c for c in set(contact)]  # initial zero fix

    td = soup.find_all('td', string='Email')
    data = td[1].parent.find_all('td')[1].text.strip()
    email = {director[3].text.strip(), data}

    td = soup.find_all('td', string=' Mailling Address ')
    data = td[0].parent.find_all('td')[1].text.strip()
    mailing_address = utils.strip_double_space(data)

    td = soup.find_all('td', string='Factory Address ')
    data = td[0].parent.find_all('td')[1].text.strip()
    factory_address = utils.strip_double_space(data)

    td = soup.find_all('td', string='Website')
    website = td[0].parent.find_all('td')[1].text.strip()

    contact = ', '.join(contact)
    email = ', '.join(set(e.strip() for e in email if '@' in e))
    mailing_address = mailing_address.replace('\n', ', ')
    factory_address = factory_address.replace('\n', ', ')

    return '{}\t{}\t{}\t{}\t{}\t{}\t{}'.format(name, designation, contact, email, mailing_address, factory_address,
                                               website)


def collect_details(resolve_error=False):
    file = file_failed if resolve_error else file_links
    if not os.path.isfile(file):
        print('\'{}\' couldn\'t found.\nPlease collect member links first!'.format(file))
        return

    with open(file, 'rt') as tsv, open(file_failed, "w", encoding='utf-8') as err_doc:
        reader = csv.reader(tsv, delimiter='\t')

        for i, row in enumerate(reader):
            # if i == 6: break
            company = row[0]
            director = row[1]
            email = row[2]
            url = row[3]

            print('\n{}/{}. URL:'.format(i + 1, 4360), url, end='')
            try:
                detail = scrap_details(url)
                line = '{}\t{}\t{}\n'.format(company, detail, url)
                with open(file_final, "a", encoding='utf-8') as doc:
                    doc.write(line)
            except:
                print('  ---FAILED----!!!', end='')
                err_doc.write('\t'.join(row) + '\n')


if __name__ == '__main__':
    # scrap_links(members_url)
    # collect_links()
    collect_details()
    # scrap_details(details_url)
